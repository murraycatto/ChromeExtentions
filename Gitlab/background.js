var frame = 'none';

// frame options
chrome.storage.sync.get(function(items) {
    frame = items.showFrame;
});

var app = function() {
    chrome.app.window.create(
        'index.html',
        {
            id: 'mainWindow',
            frame: { type: frame },
            resizable: true,
            
        },

        function(e) {
            
        }
    );
}

chrome.app.runtime.onLaunched.addListener(function() {
    app();
});

chrome.app.runtime.onRestarted.addListener(function() {
    app();
});

